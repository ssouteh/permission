Examples describe how create a simple role and rolebinding.
Also, a shell script to generate a client certificate for user indicate into role binding.

HOWTO :
First : create role and role binding, using kubeclt cli :
kubectl create -f example-role.yml
kubectl create -f example-rolebinding.yml

Second:  Create kubeconfig file which contains client certificate for user  indicate into role binding, by  executing the shell script create-client-certificate.sh:


./create-client-certificate.sh example-user example-group (script requite two parameters : user and group)

To manage cluster using this kubeconfig file :
- Install kubectl cli 
- Have access to api-server endpoint 
- Manage  cluster using --kubeconfig option : kubectl --kubeconfig=kubeconfig get pod 
